import appStorage, { AppStorage } from '../services/appStorage';


export default {
  state: {
    favorites: [],
  },

  mutations: {
    toggleFavorite(state, payload) {
      if (state.favorites.includes(payload)) {
        const index = state.favorites.indexOf(payload);
        state.favorites.splice(index, 1);
      } else {
        state.favorites.push(payload);
      }

      appStorage.setItem(AppStorage.FAVORITES, state.favorites);
    },

    setFavorites(state, payload) {
      state.favorites = payload;
      appStorage.setItem(AppStorage.FAVORITES, state.favorites);
    },
  },

  getters: {
    isFavorite: (state) => (currency) => state.favorites.includes(currency),
  },
};
