import appStorage, { AppStorage } from '../services/appStorage';

export default {
  namespaced: true,

  state: {
    theme: null,
  },

  mutations: {
    setTheme(state, payload) {
      state.theme = payload;
      appStorage.setItem(AppStorage.CURRENT_THEME, payload);
    },
  },
};
