import appStorage, { AppStorage } from '../services/appStorage';


export default {
  namespaced: true,

  state: {
    value: null,
  },

  mutations: {
    setValue(state, payload) {
      state.value = payload;
      appStorage.setItem(AppStorage.CALCULATOR_VALUE, payload);
    },
  },
};
