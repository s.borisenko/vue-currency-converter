import Vue from 'vue';
import Router from 'vue-router';

import { Route } from './routes';
import ConverterCalculator from '../components/screen/ConverterCalculator/ConverterCalculator.vue';
import CurrencySettings from '../components/screen/CurrencySettings/CurrencySettings.vue';
import Settings from '../components/screen/Settings/Settings.vue';


Vue.use(Router);

const routes = [
  {
    path: '/',
    name: Route.HOME,
    component: ConverterCalculator,
  },
  {
    path: '/currency-settings',
    name: Route.CURRENCY_SETTINGS,
    component: CurrencySettings,
  },
  {
    path: '/settings',
    name: Route.SETTINGS,
    component: Settings,
  },
  {
    path: '*',
    redirect: '/',
  },
];

export default new Router({
  mode: 'history',
  routes,
});
