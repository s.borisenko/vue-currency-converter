import Vue from 'vue';
import Vuex from 'vuex';

import calculator from './store/calculator';
import currency from './store/currency';
import favorite from './store/favorite';
import locale from './store/locale';
import settings from './store/settings';


Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    calculator,
    currency,
    favorite,
    locale,
    settings,
  },
});
