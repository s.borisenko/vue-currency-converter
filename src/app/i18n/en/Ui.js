import DictionaryMapping from '../DictionaryMapping';

export default {
  [DictionaryMapping.Ui.ENTER_VALUE]: 'Enter value',
  [DictionaryMapping.Ui.CHOOSE_CURRENCY]: 'Choose currency',
  [DictionaryMapping.Ui.CHOOSE_LANGUAGE]: 'Choose language',
  [DictionaryMapping.Ui.CHOOSE_COLOR_THEME]: 'Choose color theme',
  [DictionaryMapping.ScreenTitle.CURRENCIES]: 'Currency list',
  [DictionaryMapping.ScreenTitle.SETTINGS]: 'Settings',
  [DictionaryMapping.Tabs.CONVERTER]: 'Converter',
  [DictionaryMapping.Tabs.SETTINGS]: 'Settings',
  [DictionaryMapping.ColorTheme.DARK]: 'Dark',
  [DictionaryMapping.ColorTheme.LIGHT]: 'Light',
  [DictionaryMapping.Ui.TRY_AGAIN]: 'Try again',
  [DictionaryMapping.Ui.LOADING_ERROR]: 'Network error',
};
