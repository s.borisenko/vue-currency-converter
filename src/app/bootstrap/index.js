import appStorage, { AppStorage } from '../services/appStorage';
import store from '../store';


const boot = async () => {
  store.commit('locale/setLang', appStorage.getItem(AppStorage.CURRENT_LANG) || 'en');
  store.commit('settings/setTheme', appStorage.getItem(AppStorage.CURRENT_THEME) || 'default');
  store.commit('setFavorites', appStorage.getItem(AppStorage.FAVORITES) || []);
  store.commit('currency/setBaseCurrency', appStorage.getItem(AppStorage.BASE_CURRENCY) || 'USD');
  store.commit('calculator/setValue', appStorage.getItem(AppStorage.CALCULATOR_VALUE) || null);
};

export default boot;
