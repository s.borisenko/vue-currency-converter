import Vue from 'vue';

import router from './app/router';
import store from './app/store';
import App from './app/App.vue';
import './plugins/FontAwesome';

import './assets/styles/index.styl';
import './registerServiceWorker';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
